import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import { LoginDto } from './dto/login.dto';
import { JwtPayload } from './jwt.payload';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) { }

  async login(dto: LoginDto): Promise<string> {

    try {
      const id: number = await this.userService.verifyUser(
        dto.phoneNumber,
        dto.otp
      );
      const payload: JwtPayload = { userID: id };
      const token = this.jwtService.sign(payload);
      return token;
    }
    catch (error) {
      throw error
    }
  }
}
