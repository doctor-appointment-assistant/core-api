import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, StrategyOptions } from 'passport-jwt';
import { getJwtConfig } from 'src/config/jwt.config';
import { cookieExtractor } from './cookie-extractor.jwt-from-request-function';
import { JwtPayload } from './jwt.payload';

const jwtConfig = getJwtConfig();

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    const options: StrategyOptions = {
      jwtFromRequest: cookieExtractor,
      ignoreExpiration: false,
      secretOrKey: jwtConfig.secret,
    };
    super(options);
  }

  async validate(payload: JwtPayload) {
    return { userID: payload.userID };
  }
}
