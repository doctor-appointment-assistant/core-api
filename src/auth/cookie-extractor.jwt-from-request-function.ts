import { Request } from 'express';
import { JwtFromRequestFunction } from 'passport-jwt';

export const cookieExtractor: JwtFromRequestFunction = function (
  req: Request,
): string {
  let token: string = null;
  if (req && req.cookies) {
    token = req.cookies['jwt'];
  }
  return token;
};
