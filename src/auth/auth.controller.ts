import {
    Body,
    Controller,
    Delete,
    Get,
    Post,
    Req,
    Res,
    UseGuards,
    ValidationPipe,
  } from '@nestjs/common';
  import { Request, Response } from 'express';
  import { getJwtCookieOptions } from 'src/config/jwt.config';
  import { AuthService } from './auth.service';
  import { LoginDto } from './dto/login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtPayload } from './jwt.payload';
  
  @Controller('auth')
  export class AuthController {
    constructor(private readonly authService: AuthService) {}
  
    @Post()
    async login(
      @Body(new ValidationPipe({ whitelist: true })) dto: LoginDto,
      @Res({ passthrough: true }) res: Response,
    ): Promise<boolean> {
      const token: string = await this.authService.login(dto);
      res.cookie('jwt', token, getJwtCookieOptions());
      return true;
    }
  
    @Delete()
    async clearCookie(@Res({ passthrough: true }) res: Response): Promise<void> {
      res.clearCookie('jwt');
    }
    
    @Get()
    @UseGuards(JwtAuthGuard)
    async checkCredential(@Req() req: Request): Promise<JwtPayload> {
      return req.user;
    }
  }
  