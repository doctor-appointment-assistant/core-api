import { User } from "src/user/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity()
export class Appointment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    doctor: string;

    @Column({ name:'patient_id_1',default:null })
    patientId1: number;

    @Column({ name:'patient_id_2',default:null })
    patientId2: number;

    @Column({ name:'patient_id_3',default:null })
    patientId3: number;

    @Column({ type:'datetime', name: "begin_time"})
    beginTime: Date;

    @Column({ type:'text', default:null })
    information1: string;

    @Column({ type:'text', default:null })
    information2: string;

    @Column({ type:'text', default:null })
    information3: string;

    @Column({name: 'is_available', default: true})
    isAvailable: boolean;

    @ManyToOne(() => User)
    @JoinColumn({ name:'patient_id_1' })
    patient1: User

    @ManyToOne(() => User)
    @JoinColumn({ name:'patient_id_2' })
    patient2: User

    @ManyToOne(() => User)
    @JoinColumn({ name:'patient_id_3' })
    patient3: User
}
