import { Appointment } from "../entity/appointment.entity";

export interface GetAppointmentsInDateInterface {
    appointments: Omit<Appointment, 'patientId1' | 'patientId2' | 'patientId3' | 'information1' | 'information2' | 'information3'>[]
    patientCount: { [time: string]: number }
}