import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class BookAppointmentDto {
    @IsNotEmpty()
    @IsString()
    information: string;

    @IsNotEmpty()
    @IsNumber()
    appointmentId: number;
}