import { Type } from 'class-transformer';
import { IsArray, IsDateString, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
export class CreateAppointmentsSlotDto {
  @ValidateNested()
  @IsArray()
  @Type(() => Appointment)
  appointments: Appointment[]
}
class Appointment {
  @IsNotEmpty()
  @IsString()
  doctor: string;

  @IsNotEmpty()
  @IsDateString()
  beginTime: Date;
}
