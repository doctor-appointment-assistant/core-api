import { Get, Param, Req, UseGuards, ValidationPipe } from '@nestjs/common';
import { Post } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { JwtPayload } from 'src/auth/jwt.payload';
import { AppointmentService } from './appointment.service';
import { CreateAppointmentsSlotDto } from './dto/add-appointment.dto';
import { BookAppointmentDto } from './dto/book-appointment.dto';
import { Appointment } from './entity/appointment.entity';
import { Request } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { GetAppointmentsInDateInterface } from './dto/get-appointments.interface';

@Controller('appointment')
export class AppointmentController {
    constructor(
        private readonly appointmentService: AppointmentService
    ){}

    @Get()
    @UseGuards(JwtAuthGuard)
    async getAllAppointments(): Promise<Appointment[]>{
      return this.getAllAppointments()
    }
  
    @Get('date/:date')
    async getAppointmentsInDate(
      @Param('date')
      date: Date 
    ): Promise<GetAppointmentsInDateInterface>{
      return this.appointmentService.getAppointmentsInDate(date)
    }

    @Post('slot')
    @UseGuards(JwtAuthGuard)
    async createSlot(
      @Body(new ValidationPipe({ whitelist: true }))
      dto: CreateAppointmentsSlotDto
    ): Promise<boolean> {
      return this.appointmentService.createAppointmentSlot(dto)
    }

    @Post('booking')
    @UseGuards(JwtAuthGuard)
    async bookAppointment(
      @Body(new ValidationPipe({ whitelist: true }))
      dto: BookAppointmentDto,
      @Req() req: Request
    ): Promise<Omit<Appointment, 'patientId1' | 'patientId2' | 'patientId3' | 'information1' | 'information2' | 'information3'>> {
      return this.appointmentService.bookAppointment(dto, (req.user as JwtPayload).userID)
    }
}
