import { ConflictException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { formatTime, getNextNDay } from 'src/utils/utils';
import { Between, IsNull, Not, Repository } from 'typeorm';
import { CreateAppointmentsSlotDto } from './dto/add-appointment.dto';
import { BookAppointmentDto } from './dto/book-appointment.dto';
import { GetAppointmentsInDateInterface } from './dto/get-appointments.interface';
import { Appointment } from './entity/appointment.entity';

@Injectable()
export class AppointmentService {
    constructor(
        @InjectRepository(Appointment)
        private readonly appointmentRepository: Repository<Appointment>
    ){}

    async getAllAppointment(): Promise<Appointment[]> {
        const appointments = await this.appointmentRepository.find();
        return appointments
    }

    async getAppointmentFromId(id: number): Promise<Appointment> {
        const appointment = await this.appointmentRepository.findByIds([id])
        if (appointment.length === 1){
            return appointment[0]
        }
        throw new NotFoundException()
    }

    async getUnmadeAppointment(): Promise<Appointment[]>{
        const appointments = await this.appointmentRepository.find({
            where: {
                patient: IsNull()
            }
        })
        return appointments
    }

    async getMadeAppointment(): Promise<Appointment[]>{
        const appointments = await this.appointmentRepository.find({
            where: {
                patient: Not(IsNull())
            }
        })
        return appointments
    }

    async getAppointmentsInDate(date: Date): Promise<GetAppointmentsInDateInterface> {
        const firstDate: Date = new Date(date)
        firstDate.setHours(0,0,0,0)
        const nextDay = getNextNDay(firstDate, 1)
        const candidates = await this.appointmentRepository.find({
            order: {
                beginTime: 'ASC'
            },
            where: {
                beginTime: Between(firstDate, nextDay)
            }
        })

        let patientCount: { [time: string]: number } = {};
        for (const x of candidates) {
            console.log(x)
            if (!(formatTime(x.beginTime) in patientCount)) {
                patientCount[formatTime(x.beginTime)] = 0
            }
            if (x.patientId1 !== null) {
                patientCount[formatTime(x.beginTime)]++
            }
            if (x.patientId2 !== null) {
                patientCount[formatTime(x.beginTime)]++
            }
            if (x.patientId3 !== null) {
                patientCount[formatTime(x.beginTime)]++
            }
            
        }

        const appointments = candidates.map((appointment) => {
            const {patientId1, patientId2, patientId3, information1, information2, information3, ...rest} = appointment;
            return rest
        })

        const res: GetAppointmentsInDateInterface = { appointments: appointments, patientCount: patientCount }
        return res
    }

    async createAppointmentSlot(dto: CreateAppointmentsSlotDto): Promise<boolean> {
        try {
            for (const appointment of dto.appointments) {
                this.appointmentRepository.save(appointment)
            }
        } catch {
            throw new InternalServerErrorException('insert error')
        }
        return true;
    }

    async bookAppointment(dto: BookAppointmentDto, patientId: number): Promise<Omit<Appointment, 'patientId1' | 'patientId2' | 'patientId3' | 'information1' | 'information2' | 'information3'>> {
        try {
            const appointment = await this.getAppointmentFromId(dto.appointmentId)
            if (!appointment.isAvailable) {
                throw new ConflictException('This slot is full')
            }

            if (appointment.patientId2 !== null) {
                appointment.patientId3 = patientId
                appointment.information3 = dto.information
                appointment.isAvailable = false
            } else if (appointment.patientId1 !== null) {
                appointment.patientId2 = patientId
                appointment.information2 = dto.information
            } else {
                appointment.patientId1 = patientId
                appointment.information1 = dto.information
            }
            const {patientId1, patientId2, patientId3, information1, information2, information3, ...res} = await this.appointmentRepository.save(appointment)
            return res

        } catch(error) {
            throw error
        }
    }
}
