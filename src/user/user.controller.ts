import { Get, Param, UseGuards, ValidationPipe } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { Controller, Post } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor (
        private readonly userService: UserService
    ){}

    @Post()
    async createUser(
        @Body(new ValidationPipe({ whitelist:true })) 
        dto: CreateUserDto
        ): Promise<User> {
        return this.userService.createUser(dto)
    }

    // @Post('verify')
    // async verifyUser(
    //     @Body(new ValidationPipe({ whitelist:true })) 
    //     dto: VerifyUserDto
    //     ): Promise<boolean> {
    //     return this.userService.verifyUser(dto)
    // }

    @Get('otp/:phone')
    async getOtp(
        @Param('phone') phoneNumber: string
    ): Promise<string> {
        return this.userService.getOtp(phoneNumber)
    }

    @Get()
    @UseGuards(JwtAuthGuard)
    async getAllUser(): Promise<User[]> {
        return this.userService.getAllUser()
    }
}
