import { Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn({ name:"id" })
    id: number;

    @Column({ name:"citizen_id", length: 20, nullable: false })
    citizenId: string;

    @Column({ name:"first_name", length: 20, nullable: false })
    firstName: string;

    @Column({ name:"last_name", length: 20, nullable: false })
    lastName: string;

    @Column({ name:"phone_number", length: 20, nullable: false })
    phoneNumber: string;

    @Column( { name: "is_verified", default: false})
    isVerified: boolean;

    @CreateDateColumn({ name:"created_by" })
    createdBy: Date;
}