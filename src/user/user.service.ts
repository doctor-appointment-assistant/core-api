import { ConflictException, Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Configuration } from 'src/config/configuration';
import { OtpService } from 'src/otp/otp.service';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly otpService: OtpService,
        private readonly configService: ConfigService<Configuration>,
    ){}

    async getAllUser(): Promise<User[]> {
        return this.userRepository.find();
    }

    async findUserById(id: number): Promise<User | null> {
        const user: User[] = await this.userRepository.findByIds([id])
        if (user.length === 0) {
            return null;
        }
        return user[0]
    }

    async findUserByCitizenId(citizenId: string): Promise<User | null> {
        const user: User[] = await this.userRepository.find({
            where: {
                citizenId: citizenId
            }
        })
        if (user.length === 0) {
            return null;
        }
        return user[0]
    }

    async findUserByPhoneNumber(phoneNumber: string): Promise<User | null> {
        const user: User[] = await this.userRepository.find({
            where: {
                phoneNumber: phoneNumber
            }
        })
        if (user.length === 0) {
            return null;
        }
        return user[0]
    }

    async createUser(dto: CreateUserDto): Promise<User> {
        let user: User = await this.findUserByCitizenId(dto.citizenId)
        if (user !== null) {
            throw new ConflictException('Duplicated Citizen ID')
        }

        user = await this.findUserByPhoneNumber(dto.phoneNumber)
        if (user !== null) {
            throw new ConflictException('Phone number already used')
        }

        return this.userRepository.save({ ...dto, isVerified: false})
    }

    async verifyUser(phoneNumber: string, otp: string): Promise<number> {
        const isValidate: boolean = await this.otpService.TOP_TRANS_checkOTP(
            phoneNumber, 
            otp
        );
        if (isValidate) {
            const user = await this.findUserByPhoneNumber(phoneNumber)
            user.isVerified = true
            await this.userRepository.save(user)
            return user.id
        }
        throw new UnauthorizedException('Authorization Failed')
    }

    async getOtp(phoneNumber: string): Promise<string> {
        const user: User = await this.findUserByPhoneNumber(phoneNumber)
        if (user === null) {
            throw new UnauthorizedException('Sign up first')
        }
        // return this.otpService.requestForOTP(phoneNumber)
        return this.configService.get<boolean>('isProduction') ?
            this.otpService.requestForOTP(phoneNumber) :
            this.otpService.requestForOTPForTest(phoneNumber)
    }

}
