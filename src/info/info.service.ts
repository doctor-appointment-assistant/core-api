import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as redis from 'redis';
import { Configuration } from 'src/config/configuration';
import { promisify } from 'util';

@Injectable()
export class InfoService {
  private _redisClient: redis.RedisClient | undefined;

  constructor(private readonly configService: ConfigService<Configuration>) {}

  client(): Promise<redis.RedisClient> {
    return new Promise((resolve, reject) => {
      if (!this._redisClient) {
        this._redisClient = redis.createClient(
          this.configService.get<redis.ClientOpts>('redisConfig'),
        );

        let isFirstError = true;
        this._redisClient.on('error', (error) => {
          console.error(
            '[Nest/Redis]',
            new Date().toLocaleString(),
            'Error Creating Redis Client',
            error,
          );

          if (isFirstError) {
            isFirstError = false;
            reject(new ServiceUnavailableException());
          } else {
            this._redisClient?.quit();
            this._redisClient = undefined;
          }
        });
        this._redisClient.once('ready', () => {
          console.log(
            '[Nest/Redis]',
            new Date().toLocaleString(),
            'Created Redis Client',
          );
          resolve(this._redisClient);
        });
      } else {
        resolve(this._redisClient);
      }
    });
  }

  async get(key: string): Promise<string> {
    const client: redis.RedisClient = await this.client();
    return promisify(client.get).bind(client)(key);
  }

  async getCurrentPatients(): Promise<number> {
    const currentPatients = await this.get('currentPatients');
    return parseInt(currentPatients);
  }
}
