import {
  Injectable,
  CanActivate,
  ExecutionContext,
  MethodNotAllowedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Configuration } from 'src/config/configuration';

@Injectable()
export class DevGuard implements CanActivate {
  constructor(private readonly configService: ConfigService<Configuration>) {}
  canActivate(_context: ExecutionContext): boolean {
    if (this.configService.get<boolean>('isProduction')) {
      throw new MethodNotAllowedException();
    }

    return true;
  }
}
