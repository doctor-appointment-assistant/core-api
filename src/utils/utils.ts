export function getToday(): Date{
    const today = new Date();
    today.setHours(0,0,0,0); //set time to 00:00:00:00
    return today;
}

export function getNextNDay(date: Date, n: number): Date{
    const nextNDay = new Date(date);
    nextNDay.setDate(date.getDate() + n);
    return nextNDay;
}

export function formatTime(date:Date) {
    let hour = '' + date.getHours()
    let minute = '' +date.getMinutes()
        
    if (hour.length < 2) 
        hour = '0' + hour;
    if (minute.length < 2) 
        minute = '0' + minute;

    return [hour, minute].join(':');
} 
