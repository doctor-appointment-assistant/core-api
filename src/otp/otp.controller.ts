import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { DevGuard } from 'src/utils/dev.guard';
import { OtpService } from './otp.service';

@Controller('otp')
export class OtpController {
  constructor(private readonly otpService: OtpService) {}

  @UseGuards(DevGuard)
  @Get('test/create/:phone')
  async requestForOtp(@Param('phone') phone: string): Promise<string> {
    return this.otpService.requestForOTP(phone);
  }

  @UseGuards(DevGuard)
  @Get('test/check/:phone/:otp')
  async checkOTP(
    @Param('phone') phone: string,
    @Param('otp') otp: string,
  ): Promise<boolean> {
    return this.otpService.TOP_TRANS_checkOTP(phone, otp);
  }

  @UseGuards(DevGuard)
  @Get('test/nosms/:phone')
  async requestForOtpTest(@Param('phone') phone: string): Promise<string> {
    return this.otpService.requestForOTPForTest(phone);
  }

}
