import { GoneException, Injectable, NotFoundException } from '@nestjs/common';
import { OtpRequest } from './otp-request.entity';
import { addMinutes } from 'date-fns';
import { ConfigService } from '@nestjs/config';
import { Configuration, OtpConfig } from 'src/config/configuration';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionRepository } from 'typeorm';
import * as crypto from 'crypto';
import {
  MessageAttributeValue,
  PublishCommand,
  SNSClient,
  SNSClientConfig,
} from '@aws-sdk/client-sns';

@Injectable()
export class OtpService {
  constructor(
    private readonly configService: ConfigService<Configuration>,
    @InjectRepository(OtpRequest)
    private readonly otpRequestRepository: Repository<OtpRequest>,
  ) {}

  async requestForOTP(phoneNumber: string): Promise<string> {
    const otpAddMinutes: number = this.configService.get<OtpConfig>('otpConfig')
      .addMinutes;
    const createdDate: Date = new Date();
    const req: OtpRequest = {
      createdDate,
      validTil: addMinutes(createdDate, otpAddMinutes),
      phoneNumber,
      otp: this.generateOTP(6),
      isAlreadyUsed: false,
    };

    const res: OtpRequest = await this.otpRequestRepository.save(req);
    await this.sendSMS(res.phoneNumber, res.otp);
    return res.phoneNumber;
  }

  async requestForOTPForTest(phoneNumber: string): Promise<string> {
    const otpAddMinutes: number = this.configService.get<OtpConfig>('otpConfig')
      .addMinutes;
    const createdDate: Date = new Date();
    const req: OtpRequest = {
      createdDate,
      validTil: addMinutes(createdDate, otpAddMinutes),
      phoneNumber,
      otp: this.generateOTP(6),
      isAlreadyUsed: false,
    };

    const res: OtpRequest = await this.otpRequestRepository.save(req);
    // await this.sendSMS(res.phoneNumber, res.otp);
    return res.phoneNumber;
  }

  @Transaction()
  async TOP_TRANS_checkOTP(
    phoneNumber: string,
    otp: string,
    @TransactionRepository(OtpRequest)
    otpRequestRepository: Repository<OtpRequest> = null,
  ): Promise<boolean> {
    let req: OtpRequest;
    try {
      req = await otpRequestRepository.findOneOrFail(phoneNumber);
    } catch (_) {
      throw new NotFoundException();
    }

    if (new Date().getTime() > req.validTil.getTime() || req.isAlreadyUsed) {
      throw new GoneException();
    }

    if (req.otp != otp) {
      return false;
    }

    req.isAlreadyUsed = true;
    await otpRequestRepository.save(req);

    return true;
  }

  private generateOTP(otpLength: number): string {
    const arr = new Uint32Array(otpLength);
    crypto.randomFillSync(arr);
    let out = '';
    for (let i = 0; i < arr.length; i++) {
      out += arr[i] % 10;
    }
    return out;
  }

  private async sendSMS(phoneNumber: string, otp: string): Promise<void> {
    const client = new SNSClient(
      this.configService.get<SNSClientConfig>('snsConfig'),
    );

    const otpConfig: OtpConfig = this.configService.get<OtpConfig>('otpConfig');
    const attr: { [key: string]: MessageAttributeValue } = {
      'AWS.SNS.SMS.SMSType': {
        DataType: 'String',
        StringValue: 'Transactional',
      },
      'AWS.SNS.SMS.MaxPrice': {
        DataType: 'Number',
        StringValue: otpConfig.maxPrice,
      },
      'AWS.SNS.SMS.SenderID': {
        DataType: 'String',
        StringValue: otpConfig.senderId,
      },
    };

    const pub: PublishCommand = new PublishCommand({
      Message: `${otp} is your OTP to Doctor Appointment Assistant`,
      PhoneNumber: phoneNumber,
      MessageAttributes: attr,
    });

    await client.send(pub);
  }
}
