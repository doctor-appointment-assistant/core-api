import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class OtpRequest {
  @PrimaryColumn({ type: 'varchar', length: 20, nullable: false })
  phoneNumber: string;

  @Column({ type: 'timestamp', nullable: false })
  validTil: Date;

  @Column({ type: 'timestamp', nullable: false })
  createdDate: Date;

  @Column({ type: 'varchar', length: 10, nullable: false })
  otp: string;

  @Column({ type: 'boolean', nullable: false, default: false })
  isAlreadyUsed: boolean;
}
