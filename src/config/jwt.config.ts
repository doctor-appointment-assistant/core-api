import { CookieOptions } from 'express';
import { isProduction } from './is-production';

export interface JwtConfig {
  secret: string;
  expiresIn: string;
}

export function getJwtConfig(): JwtConfig {
  return {
    secret: process.env.JWT_SECRET || 'devSecret123',
    expiresIn: process.env.JWT_EXPIRES_IN || '2h',
  };
}

export function getJwtCookieOptions(): CookieOptions {
  const sharedOptions: CookieOptions = {
    httpOnly: true,
    // sameSite: true,
  };
  // if (isProduction()) {
  //   return {
  //     domain: 'taatek.r96.dev',
  //     secure: true,
  //     sameSite: true,
  //     ...sharedOptions,
  //   };
  // }
  return sharedOptions;
}
