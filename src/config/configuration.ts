import { SNSClientConfig } from '@aws-sdk/client-sns';
import { ClientOpts } from 'redis';

export default (): Configuration => ({
  isProduction: process.env.NODE_ENV === 'production',
  otpConfig: {
    addMinutes: parseInt(process.env.OTP_ADD_MINUTES) || 5,
    maxPrice: process.env.OTP_MAX_PRICE || '0.02',
    senderId: process.env.OTP_SENDER_ID || 'DoctorAst',
  },
  snsConfig: {
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
    region: process.env.AWS_REGION,
  },
  redisConfig: {
    host: process.env.REDIS_HOST || 'redis',
    // password: process.env.REDIS_PASS || '12345678',
    db: '0',
  },
  corsOrigin: process.env.FRONTEND_ORIGIN,
});

export interface Configuration {
  isProduction: boolean;
  otpConfig: OtpConfig;
  snsConfig: SNSClientConfig;
  redisConfig: ClientOpts;
  corsOrigin: string;
}

export interface OtpConfig {
  addMinutes: number;
  maxPrice: string;
  senderId: string;
}
